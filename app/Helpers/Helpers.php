<?php

if ( ! function_exists( 'flog' ) ) {
    function _var_dump( $var ) {
        ob_start();
        print_r( $var );
        $v = ob_get_contents();
        ob_end_clean();

        return $v;
    }

    function flog( $var ) {
        file_put_contents( storage_path('logs') . '/log-' . date( 'Y-m-d' ) . '.txt', '+---+ ' . date( 'H:i:s d-m-Y' ) . ' +-----+' . PHP_EOL . _var_dump( $var ) . PHP_EOL . PHP_EOL, FILE_APPEND );
    }
}

