<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'name',
        'email',
        'phone',
        'created',
    ];

    protected $casts = [
        'created' => 'date_format:Y-m-d H:i:s',
    ];

    public function orders()
    {
        return $this->hasMany(Order::class);
    }
}
