<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'code',
        'type',
        'subtype',
        'total',
        'fee',
        'status',
        'customer_id',
        'client_id',
        'source_id',
        'created',
    ];

    protected $casts = [
        'total' => 'decimal:3',
        'fee' => 'double',
        'created' => 'date_format:Y-m-d H:i:s',
    ];

    public static function parseAndSave(array $attributes = [])
    {
        $customerData = $attributes['customer'];
        $customer = Customer::firstOrCreate(
            ['email' => $customerData['email']],
            $customerData
        );
        $attributes['customer_id'] = $customer->id ?? 0;
        $order = parent::create($attributes);
        $order
            ->createProducts($attributes['products'] ?? [])
            ->createPayments($attributes['payments'] ?? [])
            ->createPoints($attributes['points'] ?? []);

        return $order;
    }

    public function products()
    {
        return $this->hasMany(OrderProduct::class);
    }

    public function payments()
    {
        return $this->hasMany(OrderPayment::class);
    }

    public function points()
    {
        return $this->hasMany(OrderPoint::class);
    }

    public function customer()
    {
        return $this->belongsTo(Customer::class);
    }

    public function source()
    {
        return $this->belongsTo(Source::class);
    }

    public function createProducts(array $attributes = [])
    {
        foreach($attributes as $product) {
            $product['order_id'] = $this->id;
            OrderProduct::create($product)->createModifiers($product['modifiers'] ?? []);
        }

        return $this;
    }

    public function createPayments(array $attributes = [])
    {
        foreach($attributes as $payment) {
            $payment['order_id'] = $this->id;
            OrderPayment::create($payment);
        }

        return $this;
    }

    public function createPoints(array $attributes = [])
    {
        $attributes['order_id'] = $this->id;
        $attributes['customer_id'] = $this->customer_id;
        OrderPoint::create($attributes);

        return $this;
    }

    public static function import($data)
    {
        $orders = unserialize($data);
        if (!empty($orders)) {
            foreach ($orders as $attributes) {
                $customer = $attributes['customer'];
                $customer = Customer::firstOrCreate(
                    ['email' => $customer['email']],
                    $customer
                );
                $attributes['customer_id'] = $customer->id ?? 0;
                /** @var $order Order */
                $order = Order::create($attributes);
                $order
                    ->createProducts($attributes['products'] ?? [])
                    ->createPayments($attributes['payments'] ?? [])
                    ->createPoints($attributes['points'] ?? []);
            }
        }
    }
}
