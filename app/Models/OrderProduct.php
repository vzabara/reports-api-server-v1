<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderProduct extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'title',
        'sku',
        'order_id',
        'quantity',
        'price',
        'sale_price',
        'total',
    ];

    protected $casts = [
        'quantity' => 'integer',
        'price' => 'decimal:3',
        'sale_price' => 'decimal:3',
        'total' => 'decimal:3',
    ];

    public function modifiers()
    {
        return $this->hasMany(ProductModifier::class);
    }

    public function createModifiers(array $attributes = [])
    {
        foreach($attributes as $modifier) {
            $modifier['order_product_id'] = $this->id;
            ProductModifier::create($modifier);
        }

        return $this;
    }

}
