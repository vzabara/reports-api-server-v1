<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Share extends Model
{
    protected $fillable = [
        'client_id',
        'customer_id',
        'affiliated',
        'code',
        'to',
    ];

    protected $casts = [
        'affiliated' => 'boolean',
    ];
}
