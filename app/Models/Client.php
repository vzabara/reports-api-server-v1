<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    protected $fillable = [
        'title',
        'commission',
        'fee',
        'user_id',
        'is_active',
    ];

    protected $casts = [
        'commission' => 'double',
        'fee' => 'double',
        'is_active' => 'boolean',
    ];
}
