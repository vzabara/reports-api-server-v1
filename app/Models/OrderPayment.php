<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderPayment extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'amount',
        'type',
        'operation',
        'order_id',
        'created',
    ];

    protected $casts = [
        'amount' => 'decimal:3',
        'created' => 'date_format:Y-m-d H:i:s',
    ];

    public function order()
    {
        return $this->belongsTo(Order::class);
    }
}
