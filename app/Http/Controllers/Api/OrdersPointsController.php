<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\OrderPointRequest;
use App\Http\Resources\OrderPointResource;
use App\Models\OrderPoint;
use App\Repositories\OrderPoints\OrderPointsRepository;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\CollectsResources;

/**
 * @group Order Points
 *
 * Class OrdersPointsController
 * @package App\Http\Controllers\Api
 */
class OrdersPointsController extends Controller
{
    public function __construct()
    {
        $this->repository = new OrderPointsRepository();
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     *
     * @return CollectsResources
     *
     * @authenticated
     */
    public function index(Request $request)
    {
        return OrderPointResource::collection($this->repository->filter($request)->page());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param OrderPointRequest $request
     *
     * @return OrderPointResource
     *
     * @authenticated
     */
    public function store(OrderPointRequest $request)
    {
        return new OrderPointResource(OrderPoint::create($request->all()));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\OrderPoint  $orderPoint
     *
     * @return OrderPointResource
     *
     * @authenticated
     */
    public function show(OrderPoint $orderPoint)
    {
        return new OrderPointResource($orderPoint);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param OrderPointRequest $request
     * @param  \App\Models\OrderPoint $orderPoint
     * @return OrderPointResource
     * @authenticated
     */
    public function update(OrderPointRequest $request, OrderPoint $orderPoint)
    {
        $orderPoint->update($request->all());

        return new OrderPointResource($orderPoint);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\OrderPoint  $orderPoint
     *
     * @return \Illuminate\Http\Response
     *
     * @authenticated
     */
    public function destroy(OrderPoint $orderPoint)
    {
        $orderPoint->delete();

        return response()->json_success();
    }
}
