<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\SourceRequest;
use App\Http\Resources\SourceResource;
use App\Models\Source;
use App\Repositories\Sources\SourcesRepository;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\CollectsResources;

/**
 * @group Sources
 *
 * Class SourcesController
 * @package App\Http\Controllers\Api
 */
class SourcesController extends Controller
{
    public function __construct()
    {
        $this->repository = new SourcesRepository();
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     *
     * @return CollectsResources
     *
     * @authenticated
     */
    public function index(Request $request)
    {
        return SourceResource::collection($this->repository->filter($request)->page());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param SourceRequest $request
     *
     * @return SourceResource
     *
     * @authenticated
     */
    public function store(SourceRequest $request)
    {
        return new SourceResource(Source::create($request->all()));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Source  $source
     *
     * @return SourceResource
     *
     * @authenticated
     */
    public function show(Source $source)
    {
        return new SourceResource($source);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param SourceRequest $request
     * @param  \App\Models\Source $source
     * @return SourceResource
     * @authenticated
     */
    public function update(SourceRequest $request, Source $source)
    {
        $source->update($request->all());

        return new SourceResource($source);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Source  $source
     *
     * @return \Illuminate\Http\Response
     *
     * @authenticated
     */
    public function destroy(Source $source)
    {
        $source->delete();

        return response()->json_success();
    }
}
