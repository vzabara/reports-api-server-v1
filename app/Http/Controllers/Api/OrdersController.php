<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\OrderDataTableRequest;
use App\Http\Requests\OrderImportRequest;
use App\Http\Requests\OrderRequest;
use App\Http\Resources\OrderResource;
use App\Models\Order;
use App\Repositories\Orders\OrdersRepository;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\CollectsResources;
use Illuminate\Http\Response;

/**
 * @group Orders
 *
 * Class OrdersController
 * @package App\Http\Controllers\Api
 */
class OrdersController extends Controller
{
    public function __construct()
    {
        $this->repository = new OrdersRepository();

        self::$fullInfo = request()->get('full') ?? false;
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     *
     * @return CollectsResources
     *
     * @authenticated
     *
     */
    public function index(Request $request)
    {
        return OrderResource::collection($this->repository->filter($request)->page());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param OrderRequest $request
     *
     * @return OrderResource
     *
     * @authenticated
     */
    public function store(OrderRequest $request)
    {
        return new OrderResource(Order::parseAndSave($request->all()));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Order  $order
     *
     * @return OrderResource
     *
     * @authenticated
     */
    public function show(Order $order)
    {
        return new OrderResource($order);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param OrderRequest $request
     * @param  \App\Models\Order $order
     * @return OrderResource
     * @authenticated
     */
    public function update(OrderRequest $request, Order $order)
    {
        $order->update($request->all());

        return new OrderResource($order);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Order  $order
     *
     * @return \Illuminate\Http\Response
     *
     * @authenticated
     */
    public function destroy(Order $order)
    {
        $order->delete();

        return response()->json_success();
    }

    /**
     * Show ordera by order code.
     *
     * @param  string  $orderCode
     *
     * @return CollectsResources
     *
     * @authenticated
     */
    public function find($orderCode)
    {
        return OrderResource::collection(Order::where('code', $orderCode)->get());
    }

    /**
     * Import resources.
     *
     * @param OrderImportRequest $request
     *
     * @return \Illuminate\Http\Response
     *
     * @authenticated
     */
    public function import(OrderImportRequest $request)
    {
        Order::import($request->get('data'));

        return response()->json_success();
    }

    public function dataTable(OrderDataTableRequest $request)
    {
        return OrderResource::collection($this->repository->dataTable($request));
    }
}
