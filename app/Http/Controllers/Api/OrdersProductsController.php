<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\OrderProductRequest;
use App\Http\Resources\OrderProductResource;
use App\Models\OrderProduct;
use App\Repositories\OrderProducts\OrderProductsRepository;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\CollectsResources;

/**
 * @group Order Products
 *
 * Class OrdersProductsController
 * @package App\Http\Controllers\Api
 */
class OrdersProductsController extends Controller
{
    public function __construct()
    {
        $this->repository = new OrderProductsRepository();
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     *
     * @return CollectsResources
     *
     * @authenticated
     */
    public function index(Request $request)
    {
        return OrderProductResource::collection($this->repository->filter($request)->page());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param OrderProductRequest $request
     *
     * @return OrderProductResource
     *
     * @authenticated
     */
    public function store(OrderProductRequest $request)
    {
        return new OrderProductResource(OrderProduct::create($request->all()));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\OrderProduct  $orderProduct
     *
     * @return OrderProductResource
     *
     * @authenticated
     */
    public function show(OrderProduct $orderProduct)
    {
        return new OrderProductResource($orderProduct);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param OrderProductRequest $request
     * @param  \App\Models\OrderProduct $orderProduct
     * @return OrderProductResource
     * @authenticated
     */
    public function update(OrderProductRequest $request, OrderProduct $orderProduct)
    {
        $orderProduct->update($request->all());

        return new OrderProductResource($orderProduct);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\OrderProduct  $orderProduct
     *
     * @return \Illuminate\Http\Response
     *
     * @authenticated
     */
    public function destroy(OrderProduct $orderProduct)
    {
        $orderProduct->delete();

        return response()->json_success();
    }
}
