<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\ClientRequest;
use App\Http\Resources\ClientResource;
use App\Models\Client;
use App\Repositories\Clients\ClientsRepository;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\CollectsResources;

/**
 * @group Clients
 *
 * Class ClientsController
 * @package App\Http\Controllers\Api
 */
class ClientsController extends Controller
{
    public function __construct()
    {
        $this->repository = new ClientsRepository();
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     *
     * @return CollectsResources
     *
     * @authenticated
     */
    public function index(Request $request)
    {
        return ClientResource::collection($this->repository->filter($request)->page());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param ClientRequest $request
     *
     * @return ClientResource
     *
     * @authenticated
     */
    public function store(ClientRequest $request)
    {
        return new ClientResource(Client::create($request->all()));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Client  $client
     *
     * @return ClientResource
     *
     * @authenticated
     */
    public function show(Client $client)
    {
        return new ClientResource($client);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param ClientRequest $request
     * @param  \App\Models\Client $client
     * @return ClientResource
     * @authenticated
     */
    public function update(ClientRequest $request, Client $client)
    {
        $client->update($request->all());

        return new ClientResource($client);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Client  $client
     *
     * @return \Illuminate\Http\Response
     *
     * @authenticated
     */
    public function destroy(Client $client)
    {
        $client->delete();

        return response()->json_success();
    }
}
