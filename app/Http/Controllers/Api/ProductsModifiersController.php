<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\ProductModifierRequest;
use App\Http\Resources\ProductModifierResource;
use App\Models\ProductModifier;
use App\Repositories\ProductModifiers\ProductModifiersRepository;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\CollectsResources;

/**
 * @group Product Modifiers
 *
 * Class ProductsModifiersController
 * @package App\Http\Controllers\Api
 */
class ProductsModifiersController extends Controller
{
    public function __construct()
    {
        $this->repository = new ProductModifiersRepository();
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     *
     * @return CollectsResources
     *
     * @authenticated
     */
    public function index(Request $request)
    {
        return ProductModifierResource::collection($this->repository->filter($request)->page());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param ProductModifierRequest $request
     *
     * @return ProductModifierResource
     *
     * @authenticated
     */
    public function store(ProductModifierRequest $request)
    {
        return new ProductModifierResource(ProductModifier::create($request->all()));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\ProductModifier  $productModifier
     *
     * @return ProductModifierResource
     *
     * @authenticated
     */
    public function show(ProductModifier $productModifier)
    {
        return new ProductModifierResource($productModifier);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param ProductModifierRequest $request
     * @param  \App\Models\ProductModifier $productModifier
     * @return ProductModifierResource
     * @authenticated
     */
    public function update(ProductModifierRequest $request, ProductModifier $productModifier)
    {
        $productModifier->update($request->all());

        return new ProductModifierResource($productModifier);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ProductModifier  $productModifier
     *
     * @return \Illuminate\Http\Response
     *
     * @authenticated
     */
    public function destroy(ProductModifier $productModifier)
    {
        $productModifier->delete();

        return response()->json_success();
    }
}
