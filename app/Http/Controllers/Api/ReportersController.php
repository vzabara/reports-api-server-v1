<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\UserRequest;
use App\Http\Resources\UserResource;
use App\Models\Admin;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class ReportersController extends Controller
{
    /**
     * Create new reporter.
     *
     * @param UserRequest $request
     *
     * @return UserResource
     *
     * @authenticated
     */
    public function store(UserRequest $request)
    {
        return new UserResource(
            Admin::create([
                'name' => $request->get('name'),
                'email' => $request->get('email'),
                'password' => Hash::make($request->get('password')),
                'email_verified_at' => now(),
            ])
        );
    }

    /**
     * Remove reporter by email.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     *
     * @authenticated
     */
    public function deleteByEmail(Request $request)
    {
        User::where('email', $request->get('email'))->first()->delete();

        return response()->json_success();
    }
}
