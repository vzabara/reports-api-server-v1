<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class CustomerRequest
 * @package App\Http\Requests
 *
 * @bodyParam name string Customer name. Example: John Doe
 * @bodyParam phone string Customer phone. Example: 01234567890
 * @bodyParam email string required Customer email address. Example: example@example.com
 */
class CustomerRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'max:255',
            'phone' => 'max:255',
            'email' => 'email:rfc,dns',
            'created' => 'date',
        ];
    }
}
