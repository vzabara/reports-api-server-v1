<?php

namespace App\Http\Requests;

use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

/**
 * Class OrderRequest
 * @package App\Http\Requests
 *
 * @bodyParam code string required Order code. Example: ORDER-123
 * @bodyParam type string required Order type. Example: takeaway
 * @bodyParam total numeric required Order amount. Example: 5.50
 * @bodyParam fee numeric Company fee. Example: 1.50
 * @bodyParam status string required Order status. Example: pending
 * @bodyParam customer_id integer Existing customer ID. Example: 1
 * @bodyParam source_id integer Existing source ID. Example: 1
 * @bodyParam created date required Order date. Example: 2020-02-27 10:02:03
 * @bodyParam customer array required Customer.
 * @bodyParam customer.name string Customer name. Example: John Doe.
 * @bodyParam customer.phone string Customer phone. Example: 01234567890
 * @bodyParam customer.email string Customer email address. Example: example@example.com
 * @bodyParam customer.created date Customer creation date. Example: 2020-02-27 09:00:00
 * @bodyParam products array required Order products.
 * @bodyParam products.*.title string required Product title. Example: Coffee
 * @bodyParam products.*.sku string required Product SKU. Example: COFFEE
 * @bodyParam products.*.quantity string required Product quantity number. Example: 1
 * @bodyParam products.*.price numeric required Product price. Example: 5.00
 * @bodyParam products.*.sale_price numeric required Product sale price. Example: 5.00
 * @bodyParam products.*.total numeric required Product total price. Example: 5.00
 * @bodyParam products.*.modifiers array Product modifiers.
 * @bodyParam products.*.modifiers.*.name string required Product modifier title. Example: Sugar
 * @bodyParam products.*.modifiers.*.price string required Product modifier price. Example: 0.50
 * @bodyParam payments array required Order payments.
 * @bodyParam payments.*.type string required Payment type. Example: creditcard
 * @bodyParam payments.*.amount numeric required Payment amount. Example: 5.50
 * @bodyParam payments.*.operation string required Operation. Example: incoming
 * @bodyParam payments.*.created date required Payment date. Example: 2020-02-27 10:00:00
 * @bodyParam points array Order points.
 * @bodyParam points.points_earned integer required Points earned. Example: 20
 * @bodyParam points.points_redeemed integer required Points redeemed. Example: 0
 * @bodyParam points.redemption integer required Points redemption. Example: 0
 */
class OrderRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'code' => 'required|max:100',
            'type' => 'required|max:200',
            'total' => 'required|numeric',
            'fee' => 'numeric',
            'status' => 'required|max:100',
            'client_id' => [
                'required',
                Rule::exists('clients', 'id')->where(function($query) {
                    $query->where([
                        'id' => $this->get('client_id'),
                        'user_id' => Auth::id(),
                    ]);
                })
            ],
            'source_id' => 'required|exists:sources,id',
            'created' => 'required|date',

            'customer' => 'array',
            'customer.name' => 'max:255',
            'customer.phone' => 'max:255',
            'customer.email' => 'email:rfc,dns',
            'customer.created' => 'date',

            'products' => 'required|array',
            'products.*.title' => 'required|max:200',
            'products.*.sku' => 'required|max:200',
            'products.*.quantity' => 'required|integer',
            'products.*.price' => 'required|numeric',
            'products.*.sale_price' => 'required|numeric',
            'products.*.total' => 'required|numeric',

            'products.*.modifiers' => 'array',
            'products.*.modifiers.*.name' => 'required|max:200',
            'products.*.modifiers.*.price' => 'required|numeric',

            'payments' => 'required|array',
            'payments.*.type' => 'required|max:100',
            'payments.*.amount' => 'required|numeric',
            'payments.*.operation' => 'required|max:100',
            'payments.*.created' => 'required|date',

            'points' => 'array',
            'points.points_earned' => 'required|integer',
            'points.points_redeemed' => 'required|integer',
            'points.redemption' => 'required|numeric',
        ];
    }
}
