<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class OrderProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|max:200',
            'sku' => 'required|max:200',
            'order_id' => 'required|exists:orders,id',
            'quantity' => 'required|integer',
            'price' => 'required|numeric',
            'sale_price' => 'required|numeric',
            'total' => 'required|numeric',
        ];
    }
}
