<?php
/**
 * Created by PhpStorm.
 * User: Vladimir Zabara <wlady2001@gmail.com>
 * Date: 10/21/19
 * Time: 3:59 PM
 */

namespace App\Repositories;

use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Builder;

trait Filterable
{
    private static $page = 1;
    private static $perPage = 15;

    private static $query;
    private static $class;

    public function filter(Request $request)
    {
        self::$class = new \ReflectionClass(__CLASS__);
        $model       = self::MODEL;
        self::$query =
            static::applyDecoratorsFromRequest(
                $request, (new $model)->newQuery()
            );

        return $this;
    }

    private static function applyDecoratorsFromRequest(Request $request, Builder $query)
    {
        if ($request->has('filter')) {
            $filters = json_decode($request->get('filter'));
            foreach ($filters as $filter) {
                $decorator = static::createFilterDecorator($filter->property);

                if (static::isValidDecorator($decorator)) {
                    $query = (new $decorator($filter->operator, $filter->value))->apply($query);
                }

            }
        }

        if ($request->has('sort')) {
            $sorters = json_decode($request->get('sort'));
            foreach ($sorters as $sorter) {
                $decorator = static::createSorterDecorator($sorter->property);

                if (static::isValidDecorator($decorator)) {
                    $query = (new $decorator($sorter->direction))->apply($query);
                }
            }
        }

        if ($request->has('page')) {
            self::$page = $request->get('page');
        }
        if ($request->has('limit')) {
            self::$perPage = $request->get('limit');
        }

        return $query;
    }

    private static function createFilterDecorator($name)
    {
        return self::$class->getNamespaceName() . '\\Filters\\' .
               str_replace(' ', '',
                   ucwords(str_replace('_', ' ', $name)));
    }

    private static function createSorterDecorator($name)
    {
        return self::$class->getNamespaceName() . '\\Sorters\\' .
               str_replace(' ', '',
                   ucwords(str_replace('_', ' ', $name)));
    }

    private static function isValidDecorator($decorator)
    {
        return class_exists($decorator);
    }

    public function list()
    {
        $model = self::MODEL;
        self::$query = (new $model)->newQuery();

        return $this;
    }

    public function page()
    {
        return self::$query->paginate(self::$perPage, ['*'], 'page', self::$page);
    }
}