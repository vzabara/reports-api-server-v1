<?php
/**
 * Created by PhpStorm.
 * User: Vladimir Zabara <wlady2001@gmail.com>
 * Date: 10/21/19
 * Time: 3:59 PM
 */

namespace App\Repositories\Clients;

use App\Models\Client;
use App\Repositories\BaseRepository;

class ClientsRepository extends BaseRepository
{
    protected static $model = Client::class;
}