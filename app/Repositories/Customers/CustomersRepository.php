<?php
/**
 * Created by PhpStorm.
 * User: Vladimir Zabara <wlady2001@gmail.com>
 * Date: 10/21/19
 * Time: 3:59 PM
 */

namespace App\Repositories\Customers;

use App\Models\Customer;
use App\Repositories\BaseRepository;

class CustomersRepository extends BaseRepository
{
    protected static $model = Customer::class;
}