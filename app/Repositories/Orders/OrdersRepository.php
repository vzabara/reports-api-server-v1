<?php
/**
 * Created by PhpStorm.
 * User: Vladimir Zabara <wlady2001@gmail.com>
 * Date: 10/21/19
 * Time: 3:59 PM
 */

namespace App\Repositories\Orders;

use App\Models\Order;
use App\Repositories\BaseRepository;

class OrdersRepository extends BaseRepository
{
    protected static $model = Order::class;
}