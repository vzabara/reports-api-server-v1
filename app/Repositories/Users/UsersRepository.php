<?php
/**
 * Created by PhpStorm.
 * User: Vladimir Zabara <wlady2001@gmail.com>
 * Date: 10/21/19
 * Time: 3:59 PM
 */

namespace App\Repositories\Users;

use App\Models\User;
use App\Repositories\BaseRepository;

class UsersRepository extends BaseRepository
{
    protected static $model = User::class;
}