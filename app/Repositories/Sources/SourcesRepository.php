<?php
/**
 * Created by PhpStorm.
 * User: Vladimir Zabara <wlady2001@gmail.com>
 * Date: 10/21/19
 * Time: 3:59 PM
 */

namespace App\Repositories\Sources;

use App\Models\Client;
use App\Repositories\BaseRepository;

class SourcesRepository extends BaseRepository
{
    protected static $model = Client::class;
}