<?php
/**
 * Created by PhpStorm.
 * User: Vladimir Zabara <wlady2001@gmail.com>
 * Date: 10/21/19
 * Time: 3:55 PM
 */

namespace App\Repositories\OrderPoints\Sorters;

use App\Repositories\Sorter;
use Illuminate\Database\Eloquent\Builder;

class Redemption extends Sorter
{
    /**
     * Apply a given search value to the builder instance.
     *
     * @param Builder $builder
     *
     * @return Builder $builder
     */
    public function apply(Builder $builder)
    {
        return $builder->orderBy('redemption', $this->direction);
    }
}