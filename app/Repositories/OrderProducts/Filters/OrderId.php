<?php
/**
 * Created by PhpStorm.
 * User: Vladimir Zabara <wlady2001@gmail.com>
 * Date: 10/21/19
 * Time: 3:55 PM
 */

namespace App\Repositories\OrderProducts\Filters;

use App\Repositories\Filter;
use Illuminate\Database\Eloquent\Builder;

class OrderId extends Filter
{
    /**
     * Apply a given search value to the builder instance.
     *
     * @param Builder $builder

     * @return Builder $builder
     */
    public function apply(Builder $builder)
    {
        return $builder->where('order_id', $this->operator, $this->value);
    }
}