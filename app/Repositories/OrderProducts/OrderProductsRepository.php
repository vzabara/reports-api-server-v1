<?php
/**
 * Created by PhpStorm.
 * User: Vladimir Zabara <wlady2001@gmail.com>
 * Date: 10/21/19
 * Time: 3:59 PM
 */

namespace App\Repositories\OrderProducts;

use App\Models\OrderProduct;
use App\Repositories\BaseRepository;

class OrderProductsRepository extends BaseRepository
{
    protected static $model = OrderProduct::class;
}