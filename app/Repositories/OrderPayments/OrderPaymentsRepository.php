<?php
/**
 * Created by PhpStorm.
 * User: Vladimir Zabara <wlady2001@gmail.com>
 * Date: 10/21/19
 * Time: 3:59 PM
 */

namespace App\Repositories\OrderPayments;

use App\Models\OrderPayment;
use App\Repositories\BaseRepository;

class OrderPaymentsRepository extends BaseRepository
{
    protected static $model = OrderPayment::class;
}