<?php
/**
 * Created by PhpStorm.
 * User: Vladimir Zabara <wlady2001@gmail.com>
 * Date: 10/22/19
 * Time: 10:15 AM
 */

namespace App\Repositories;

use Illuminate\Database\Eloquent\Builder;

abstract class Filter
{
    protected $operator;
    protected $value;

    public function __construct($operator = '=', $value = '')
    {
        switch ($operator) {
            case 'like':
                $this->value = '%' . $value . '%';
                break;
            case 'eq':
                $this->value = $value;
                $operator = '=';
                break;
            case 'ge':
                $this->value = $value;
                $operator = '>=';
                break;
            case 'gt':
                $this->value = $value;
                $operator = '>';
                break;
            case 'le':
                $this->value = $value;
                $operator = '<=';
                break;
            case 'lt':
                $this->value = $value;
                $operator = '<';
                break;
            default:
                $this->value = is_bool($value) ? ($value ? 1 : 0) : $value;
                break;
        }
        $this->operator = $operator;

        return $this;
    }

    /**
     * Apply a given search value to the builder instance.
     *
     * @param Builder $builder

     * @return Builder $builder
     */
    abstract public function apply(Builder $builder);
}