<?php

namespace App\Providers;

use Illuminate\Support\Facades\Response;
use Illuminate\Support\ServiceProvider;
use Illuminate\Http\Response as HttpResponse;

class ResponseMacroServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        Response::macro('json_success', function ($message = '') {
            return Response::make([
                'success' => true,
            ]);
        });

        Response::macro('json_error', function (\Exception $exception = null, $code = null) {
            if (is_null($exception)) {
                return Response::make([
                    'success' => false,
                ], $code ?? HttpResponse::HTTP_INTERNAL_SERVER_ERROR);
            } else if (method_exists($exception, 'getStatusCode')) {
                return Response::make([
                    'success' => false,
                    'message' => HttpResponse::$statusTexts[$exception->getStatusCode()],
                ], $exception->getStatusCode());
            } else {
                return Response::make([
                    'success' => false,
                    'message' => $exception->getMessage(),
                ], $code ?? HttpResponse::HTTP_INTERNAL_SERVER_ERROR);
            }
        });
    }
}
