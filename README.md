## Reports API Server

Reports API server have to be installed in **api-app** directory. Public directory is changed to **public_html** and directory structure is supposed to be as follows:

```
/home
  /user
    /api-app
      /app
      /bootstrap
      ...
    /public_html
```

After installation copy contents of **api/public** to **public_html** or make it a soft link.


### Initial installation

```sh
composer create-project laravel/laravel .
composer require laravel/passport
```

Create **app\Models** directory and move **App\User** model to **App\Models\User**.

Add the **Laravel\Passport\HasApiTokens** trait to **App\Models\User** model.

In **config/auth.php** configuration file set the **driver** option of the **api** authentication guard to **passport**. 

Add call to **Passport::routes()** in **AuthServiceProvider::boot()** method.


### Init DB

```sh
./artisan migrate
./artisan passport:install
./artisan db:seed
```
