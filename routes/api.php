<?php

use Illuminate\Support\Facades\Route;

// Allowed to registered users (OAuth2 authorization)
Route::prefix('v1')->middleware('auth:api')->name('api.')->group(function() {
    Route::apiResources([
        'users' => 'Api\UsersController',
        'clients' => 'Api\ClientsController',
        'customers' => 'Api\CustomersController',
        'orders' => 'Api\OrdersController',
        'order-payments' => 'Api\OrdersPaymentsController',
        'order-points' => 'Api\OrdersPointsController',
        'order-products' => 'Api\OrdersProductsController',
        'product-modifiers' => 'Api\ProductsModifiersController',
        'sources' => 'Api\SourcesController',
    ]);
    // full info
    Route::get('orders/find/{order_code}', 'Api\OrdersController@find')->name('orders.find');
});

// Allowed to registered clients (api_token authorization)
Route::prefix('v1')->middleware('auth:clients')->name('api.')->group(function() {
    Route::post('customers/find', 'Api\CustomersController@find')->name('customers.find');
    Route::get('client/{client}', 'Api\ClientsController@show')->name('client.details'); // special route
    Route::post('orders/datatable', 'Api\OrdersController@dataTable')->name('orders.datatable');
    Route::post('orders/import', 'Api\OrdersController@import')->name('orders.import');
});

// VirtualMin automation (api_token authorization)
Route::prefix('v1')->middleware('auth:virtualmin')->name('api.')->group(function() {
    Route::post('reporters', 'Api\ReportersController@store')->name('reporters.store');
    Route::post('reporters/delete', 'Api\ReportersController@deleteByEmail')->name('reporters.delete');
});
