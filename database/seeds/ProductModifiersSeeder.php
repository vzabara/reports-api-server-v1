<?php

use App\Models\ProductModifier;
use Illuminate\Database\Seeder;

class ProductModifiersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        ProductModifier::create([
            'name' => 'TEST-MODIFIER',
            'price' => 100.001,
            'order_product_id' => 1,
        ]);
    }
}
