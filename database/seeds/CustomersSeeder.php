<?php

use App\Models\Customer;
use Illuminate\Database\Seeder;

class CustomersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Customer::create([
            'id' => 1,
            'name' => 'Support',
            'email' => 'itsupport@intelagy.com',
            'created' => date('Y-m-d H:i:s'),
        ]);
    }
}
