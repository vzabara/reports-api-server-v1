<?php

use App\Models\OrderProduct;
use Illuminate\Database\Seeder;

class OrderProductsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        OrderProduct::create([
            'title' => 'TEST-PRODUCT',
            'sku' => 'TEST-SKU',
            'order_id' => 1,
            'quantity' => 1,
            'price' => 100.001,
            'sale_price' => 100.001,
            'total' => 101.101,
        ]);
    }
}
