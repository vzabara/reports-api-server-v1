<?php

use App\Models\Source;
use Illuminate\Database\Seeder;

class SourcesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Source::create([
            'id' => 1,
            'title' => 'In Place',
        ]);
        Source::create([
            'id' => 2,
            'title' => 'Online Store',
        ]);
        Source::create([
            'id' => 3,
            'title' => 'GrubHub',
        ]);
        Source::create([
            'id' => 4,
            'title' => 'Seemless',
        ]);
        Source::create([
            'id' => 5,
            'title' => 'Mobile App',
        ]);
    }
}
