<?php

use App\Models\Client;
use Illuminate\Database\Seeder;

class ClientsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Client::create([
            'id' => 1,
            'user_id' => 1,
            'title' => 'Test Store 1',
            'commission' => 0,
        ]);
        Client::create([
            'id' => 2,
            'user_id' => 1,
            'title' => 'Test Store 2',
            'commission' => 5,
        ]);
        Client::create([
            'id' => 3,
            'user_id' => 1,
            'title' => 'Test Store 3',
            'commission' => 11.5,
        ]);
    }
}
