<?php

use App\Models\OrderPoint;
use Illuminate\Database\Seeder;

class OrderPointsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        OrderPoint::create([
            'order_id' => 1,
            'customer_id' => 1,
            'points_earned' => 1,
            'points_redeemed' => 0,
            'redemption' => 0,
        ]);
    }
}
