<?php

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        factory(User::class)->create([
            'name' => 'Support',
            'email' => 'itsupport@intelagy.com',
            'privileged' => 1,
        ]);
        $this->call([
            ClientsSeeder::class,
            SourcesSeeder::class,
            CustomersSeeder::class,
            OrdersSeeder::class,
            OrderProductsSeeder::class,
            OrderPointsSeeder::class,
            ProductModifiersSeeder::class,
            OrderPaymentsSeeder::class,
        ]);
    }
}
