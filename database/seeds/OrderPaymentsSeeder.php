<?php

use App\Models\OrderPayment;
use Illuminate\Database\Seeder;

class OrderPaymentsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        OrderPayment::create([
            'type' => 'creditcard',
            'amount' => 100.001,
            'operation' => 'incoming',
            'order_id' => 1,
            'created' => date('Y-m-d H:i:s'),
        ]);
    }
}
