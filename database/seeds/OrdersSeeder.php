<?php

use App\Models\Order;
use Illuminate\Database\Seeder;

class OrdersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Order::create([
            'code' => 'TEST-ORDER',
            'type' => 'test',
            'total' => 100.001,
            'fee' => 2.5,
            'status' => 'test',
            'customer_id' => 1,
            'client_id' => 1,
            'source_id' => 1,
            'created' => date('Y-m-d H:i:s'),
        ]);
    }
}
