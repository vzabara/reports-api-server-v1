<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('code', 100);
            $table->string('type', 200)->comment('takeaway, delivery, drive-in, etc');
            $table->string('subtype', 200)->nullable();
            $table->decimal('total', 15, 3);
            $table->string('status', 100)->comment('pending,completed,etc');
            $table->bigInteger('customer_id')->unsigned();
            $table->bigInteger('client_id')->unsigned();
            $table->bigInteger('source_id')->unsigned();
            $table->dateTime('created');
            $table->foreign('customer_id')
                ->references('id')->on('customers')
                ->onDelete('cascade');
            $table->foreign('client_id')
                ->references('id')->on('clients')
                ->onDelete('cascade');
            $table->foreign('source_id')
                ->references('id')->on('sources')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('orders');
    }
}
