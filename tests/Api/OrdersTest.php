<?php

namespace Tests\Api;

class OrdersTest extends ApiTestCase
{
    public function testIndex()
    {
        $response = $this->getJson(route('api.orders.index'));
        $response
            ->assertStatus(200)
            ->assertJson([
                'data' => [],
                'links' => [],
                'meta' => [],
            ]);
    }

    public function testCreate()
    {
        $response = $this->postJson(route('api.orders.store'), [
            'code' => 'phpunit',
            'type' => 'phpunit',
            'total' => 1.05,
            'fee' => 0.05,
            'status' => 'phpunit',
            'client_id' => 1,
            'source_id' => 1,
            'created' => date('Y-m-d H:i:s'),
            'customer' => [
                'name' => 'name',
                'phone' => '1234567890',
                'email' => 'phpunit@example.com',
                'created' => date('Y-m-d H:i:s'),
            ],
            'products' => [
                [
                    'title' => 'apple',
                    'sku' => 'APPLE',
                    'quantity' => 5,
                    'price' => 2.00,
                    'sale_price' => 2.00,
                    'total' => 10.00,
                    'modifiers' => [
                        [
                            'name' => 'fresh',
                            'price' => 1.00
                        ]
                    ]
                ],
                [
                    'title' => 'orange',
                    'sku' => 'ORANGE',
                    'quantity' => 7,
                    'price' => 3.00,
                    'sale_price' => 3.00,
                    'total' => 21.00
                ]
            ],
            'payments' => [
                [
                    'type' => 'cash',
                    'amount' => 100,
                    'operation' => 'incoming',
                    'created' => date('Y-m-d H:i:s'),
                ]
            ],
            'points' => [
                'points_earned' => 12,
                'points_redeemed' => 0,
                'redemption' => 0
            ]
        ]);
        $response
            ->assertStatus(201)
            ->assertJson([
                'data' => [
                    'code' => 'phpunit',
                    'type' => 'phpunit',
                    'total' => 1.05,
                    'fee' => 0.05,
                    'status' => 'phpunit',
                    'client_id' => 1,
                    'source_id' => 1,
                ],
            ]);
        $res = json_decode($response->getContent());

        return $res->data->id;
    }

    /**
     * @depends testCreate
     *
     * @param int $id
     *
     * @return mixed
     */
    public function testUpdate(int $id)
    {
        $response = $this->patchJson(route('api.orders.update', ['order' => $id]), [
            'code' => 'phpunit',
            'type' => 'phpunit',
            'total' => 1.15,
            'fee' => 0.15,
            'status' => 'phpunit',
            'client_id' => 1,
            'source_id' => 1,
            'created' => date('Y-m-d H:i:s'),
            'customer' => [
                'name' => 'name',
                'phone' => '1234567890',
                'email' => 'phpunit@example.com',
                'created' => date('Y-m-d H:i:s'),
            ],
            'products' => [
                [
                    'title' => 'apple',
                    'sku' => 'APPLE',
                    'quantity' => 5,
                    'price' => 2.00,
                    'sale_price' => 2.00,
                    'total' => 10.00,
                    'modifiers' => [
                        [
                            'name' => 'fresh',
                            'price' => 1.00
                        ]
                    ]
                ],
                [
                    'title' => 'orange',
                    'sku' => 'ORANGE',
                    'quantity' => 7,
                    'price' => 3.00,
                    'sale_price' => 3.00,
                    'total' => 21.00
                ]
            ],
            'payments' => [
                [
                    'type' => 'cash',
                    'amount' => 100,
                    'operation' => 'incoming',
                    'created' => date('Y-m-d H:i:s'),
                ]
            ],
            'points' => [
                'points_earned' => 12,
                'points_redeemed' => 0,
                'redemption' => 0
            ]
        ]);
        $response
            ->assertStatus(200)
            ->assertJson([
                'data' => [
                    'code' => 'phpunit',
                    'type' => 'phpunit',
                    'total' => 1.15,
                    'fee' => 0.15,
                    'status' => 'phpunit',
                    'client_id' => 1,
                    'source_id' => 1,
                ],
            ]);

        return $id;
    }

    /**
     * @depends testUpdate
     *
     * @param int $id
     *
     * @return mixed
     */
    public function testShow(int $id)
    {
        $response = $this->getJson(route('api.orders.show', ['order' => $id]));
        $response
            ->assertStatus(200)
            ->assertJson([
                'data' => [
                    'code' => 'phpunit',
                    'type' => 'phpunit',
                    'total' => 1.15,
                    'fee' => 0.15,
                    'status' => 'phpunit',
                    'client_id' => 1,
                    'source_id' => 1,
                ],
            ]);
        $response = json_decode($response->getContent());

        return $response->data->code;
    }

    /**
     * @depends testShow
     *
     * @param string $code
     *
     * @return mixed
     */
    public function testFind($code)
    {
        $response = $this->getJson(route('api.orders.find', ['order_code' => $code]));
        $response
            ->assertStatus(200)
            ->assertJson([
                'data' => [ // method find returns array
                    [
                        'code' => $code,
                        'type' => 'phpunit',
                        'total' => 1.15,
                        'fee' => 0.15,
                        'status' => 'phpunit',
                        'client_id' => 1,
                        'source_id' => 1,
                    ],
                ],
            ]);
        $response = json_decode($response->getContent());

        return $response->data[0]->id;
    }

    /**
     * @depends testFind
     *
     * @param int $id
     *
     * @return mixed
     */
    public function testDelete(int $id)
    {
        $response = $this->deleteJson(route('api.orders.destroy', ['order' => $id]));
        $response
            ->assertStatus(200)
            ->assertJson([
                'success' => true,
            ]);
    }

    /**
     * @return mixed
     */
    public function testImport()
    {
        $response = $this->postJson(route('api.orders.import'), [
            'data' => file_get_contents(database_path('seeds/orders-serialized.txt')),
        ], [
            'Authorization' => 'Bearer ' . $this->user->api_token
        ]);
        $response
            ->assertStatus(200)
            ->assertJson([
                'success' => true,
            ]);
    }
}
