<?php

namespace Tests\Api;

class SourcesTest extends ApiTestCase
{
    public function testIndex()
    {
        $response = $this->getJson(route('api.sources.index'));
        $response
            ->assertStatus(200)
            ->assertJson([
                'data'  => [],
                'links' => [],
                'meta'  => [],
            ]);
    }

    public function testCreate()
    {
        $response = $this->postJson(route('api.sources.store'), [
            'title' => 'phpunit',
        ]);
        $response
            ->assertStatus(201)
            ->assertJson([
                'data' => [
                    'title' => 'phpunit',
                ],
            ]);
        $res = json_decode($response->getContent());

        return $res->data->id;
    }

    /**
     * @depends testCreate
     *
     * @param int $id
     *
     * @return mixed
     */
    public function testUpdate(int $id)
    {
        $response = $this->patchJson(route('api.sources.update', ['source' => $id]), [
            'title' => 'phpunit updated',
        ]);
        $response
            ->assertStatus(200)
            ->assertJson([
                'data' => [
                    'title' => 'phpunit updated',
                ],
            ]);

        return $id;
    }

    /**
     * @depends testUpdate
     *
     * @param int $id
     *
     * @return mixed
     */
    public function testShow(int $id)
    {
        $response = $this->getJson(route('api.sources.show', ['source' => $id]));
        $response
            ->assertStatus(200)
            ->assertJson([
                'data' => [
                    'title' => 'phpunit updated',
                ],
            ]);

        return $id;
    }

    /**
     * @depends testShow
     *
     * @param int $id
     *
     * @return mixed
     */
    public function testDelete(int $id)
    {
        $response = $this->deleteJson(route('api.sources.destroy', ['source' => $id]));
        $response
            ->assertStatus(200)
            ->assertJson([
                'success' => true,
            ]);
    }
}
