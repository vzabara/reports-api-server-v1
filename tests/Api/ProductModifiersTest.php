<?php

namespace Tests\Api;

class ProductModifiersTest extends ApiTestCase
{
    public function testIndex()
    {
        $response = $this->getJson(route('api.product-modifiers.index'));
        $response
            ->assertStatus(200)
            ->assertJson([
                'data'  => [],
                'links' => [],
                'meta'  => [],
            ]);
    }

    public function testCreate()
    {
        $response = $this->postJson(route('api.product-modifiers.store'), [
            'name' => 'phpunit',
            'price' => 1.05,
            'order_product_id' => 1,
        ]);
        $response
            ->assertStatus(201)
            ->assertJson([
                'data' => [
                    'name' => 'phpunit',
                    'price' => '1.050',
                    'order_product_id' => 1,
                ],
            ]);
        $res = json_decode($response->getContent());

        return $res->data->id;
    }

    /**
     * @depends testCreate
     *
     * @param int $id
     *
     * @return mixed
     */
    public function testUpdate(int $id)
    {
        $response = $this->patchJson(route('api.product-modifiers.update', ['product_modifier' => $id]), [
            'name' => 'phpunit',
            'price' => 1.15,
            'order_product_id' => 1,
        ]);
        $response
            ->assertStatus(200)
            ->assertJson([
                'data' => [
                    'name' => 'phpunit',
                    'price' => '1.150',
                    'order_product_id' => 1,
                ],
            ]);

        return $id;
    }

    /**
     * @depends testUpdate
     *
     * @param int $id
     *
     * @return mixed
     */
    public function testShow(int $id)
    {
        $response = $this->getJson(route('api.product-modifiers.show', ['product_modifier' => $id]));
        $response
            ->assertStatus(200)
            ->assertJson([
                'data' => [
                    'name' => 'phpunit',
                    'price' => '1.150',
                    'order_product_id' => 1,
                ],
            ]);

        return $id;
    }

    /**
     * @depends testShow
     *
     * @param int $id
     *
     * @return mixed
     */
    public function testDelete(int $id)
    {
        $response = $this->deleteJson(route('api.product-modifiers.destroy', ['product_modifier' => $id]));
        $response
            ->assertStatus(200)
            ->assertJson([
                'success' => true,
            ]);
    }
}
