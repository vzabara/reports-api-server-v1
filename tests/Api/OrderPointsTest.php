<?php

namespace Tests\Api;

class OrderPointsTest extends ApiTestCase
{
    public function testIndex()
    {
        $response = $this->getJson(route('api.order-points.index'));
        $response
            ->assertStatus(200)
            ->assertJson([
                'data'  => [],
                'links' => [],
                'meta'  => [],
            ]);
    }

    public function testCreate()
    {
        $response = $this->postJson(route('api.order-points.store'), [
            'order_id' => 1,
            'customer_id' => 1,
            'points_earned' => 1,
            'points_redeemed' => 0,
            'redemption' => 0,
        ]);
        $response
            ->assertStatus(201)
            ->assertJson([
                'data' => [
                    'order_id' => 1,
                    'customer_id' => 1,
                    'points_earned' => 1,
                    'points_redeemed' => 0,
                    'redemption' => '0.000',
                ],
            ]);
        $res = json_decode($response->getContent());

        return $res->data->id;
    }

    /**
     * @depends testCreate
     *
     * @param int $id
     *
     * @return mixed
     */
    public function testUpdate(int $id)
    {
        $response = $this->patchJson(route('api.order-points.update', ['order_point' => $id]), [
            'order_id' => 1,
            'customer_id' => 1,
            'points_earned' => 1,
            'points_redeemed' => 1,
            'redemption' => 1,
        ]);
        $response
            ->assertStatus(200)
            ->assertJson([
                'data' => [
                    'order_id' => 1,
                    'customer_id' => 1,
                    'points_earned' => 1,
                    'points_redeemed' => 1,
                    'redemption' => '1.000',
                ],
            ]);

        return $id;
    }

    /**
     * @depends testUpdate
     *
     * @param int $id
     *
     * @return mixed
     */
    public function testShow(int $id)
    {
        $response = $this->getJson(route('api.order-points.show', ['order_point' => $id]));
        $response
            ->assertStatus(200)
            ->assertJson([
                'data' => [
                    'order_id' => 1,
                    'customer_id' => 1,
                    'points_earned' => 1,
                    'points_redeemed' => 1,
                    'redemption' => '1.000',
                ],
            ]);

        return $id;
    }

    /**
     * @depends testShow
     *
     * @param int $id
     *
     * @return mixed
     */
    public function testDelete(int $id)
    {
        $response = $this->deleteJson(route('api.order-points.destroy', ['order_point' => $id]));
        $response
            ->assertStatus(200)
            ->assertJson([
                'success' => true,
            ]);
    }
}
