<?php

namespace Tests\Api;

use Illuminate\Support\Facades\Hash;

class ReportersTest extends ApiTestCase
{
    public function testCreate()
    {
        $response = $this->postJson(route('api.reporters.store'), [
            'name' => 'phpunit user',
            'email' => 'phpunit@example.com',
            'password' => Hash::make('password'),
            'email_verified_at' => now(),
        ], [
            'Authorization' => 'Bearer ' . $this->user->api_token
        ]);
        $response
            ->assertStatus(201)
            ->assertJson([
                'data' => [
                    'name' => 'phpunit user',
                    'email' => 'phpunit@example.com',
                ],
            ]);
    }

    public function testDelete()
    {
        $response = $this->postJson(route('api.reporters.delete'), [
            'email' => 'phpunit@example.com',
        ], [
            'Authorization' => 'Bearer ' . $this->user->api_token
        ]);
        $response
            ->assertStatus(200)
            ->assertJson([
                'success' => true,
            ]);
    }
}
