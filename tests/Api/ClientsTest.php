<?php

namespace Tests\Api;

class ClientsTest extends ApiTestCase
{
    public function testIndex()
    {
        $response = $this->getJson(route('api.clients.index'));
        $response
            ->assertStatus(200)
            ->assertJson([
                'data'  => [],
                'links' => [],
                'meta'  => [],
            ]);
    }

    public function testCreate()
    {
        $response = $this->postJson(route('api.clients.store'), [
            'title' => 'phpunit',
            'commission' => 5,
            'fee' => 2.5,
            'user_id' => 1,
        ]);
        $response
            ->assertStatus(201)
            ->assertJson([
                'data' => [
                    'title' => 'phpunit',
                    'commission' => 5,
                    'fee' => 2.5,
                ],
            ]);
        $res = json_decode($response->getContent());

        return $res->data->id;
    }

    /**
     * @depends testCreate
     *
     * @param int $id
     *
     * @return mixed
     */
    public function testUpdate(int $id)
    {
        $response = $this->patchJson(route('api.clients.update', ['client' => $id]), [
            'title' => 'phpunit updated',
            'commission' => 15,
            'fee' => 3.5,
            'user_id' => 1,
        ]);
        $response
            ->assertStatus(200)
            ->assertJson([
                'data' => [
                    'title' => 'phpunit updated',
                    'commission' => 15,
                    'fee' => 3.5,
                ],
            ]);

        return $id;
    }

    /**
     * @depends testUpdate
     *
     * @param int $id
     *
     * @return mixed
     */
    public function testShow(int $id)
    {
        $response = $this->getJson(route('api.clients.show', ['client' => $id]));
        $response
            ->assertStatus(200)
            ->assertJson([
                'data' => [
                    'title' => 'phpunit updated',
                    'commission' => 15,
                    'fee' => 3.5,
                ],
            ]);

        return $id;
    }

    /**
     * @depends testShow
     *
     * @param int $id
     *
     * @return mixed
     */
    public function testDelete(int $id)
    {
        $response = $this->deleteJson(route('api.clients.destroy', ['client' => $id]));
        $response
            ->assertStatus(200)
            ->assertJson([
                'success' => true,
            ]);
    }
}
