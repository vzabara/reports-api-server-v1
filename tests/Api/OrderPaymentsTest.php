<?php

namespace Tests\Api;

class OrderPaymentsTest extends ApiTestCase
{
    public function testIndex()
    {
        $response = $this->getJson(route('api.order-payments.index'));
        $response
            ->assertStatus(200)
            ->assertJson([
                'data'  => [],
                'links' => [],
                'meta'  => [],
            ]);
    }

    public function testCreate()
    {
        $response = $this->postJson(route('api.order-payments.store'), [
            'type' => 'phpunit',
            'amount' => 1.05,
            'operation' => 'incoming',
            'order_id' => 1,
            'created' => date('Y-m-d H:i:s'),
        ]);
        $response
            ->assertStatus(201)
            ->assertJson([
                'data' => [
                    'type' => 'phpunit',
                    'amount' => '1.050',
                    'operation' => 'incoming',
                    'order_id' => 1,
                ],
            ]);
        $res = json_decode($response->getContent());

        return $res->data->id;
    }

    /**
     * @depends testCreate
     *
     * @param int $id
     *
     * @return mixed
     */
    public function testUpdate(int $id)
    {
        $response = $this->patchJson(route('api.order-payments.update', ['order_payment' => $id]), [
            'type' => 'phpunit',
            'amount' => 1.15,
            'operation' => 'incoming',
            'order_id' => 1,
            'created' => date('Y-m-d H:i:s'),
        ]);
        $response
            ->assertStatus(200)
            ->assertJson([
                'data' => [
                    'type' => 'phpunit',
                    'amount' => '1.150',
                    'operation' => 'incoming',
                    'order_id' => 1,
                ],
            ]);

        return $id;
    }

    /**
     * @depends testUpdate
     *
     * @param int $id
     *
     * @return mixed
     */
    public function testShow(int $id)
    {
        $response = $this->getJson(route('api.order-payments.show', ['order_payment' => $id]));
        $response
            ->assertStatus(200)
            ->assertJson([
                'data' => [
                    'type' => 'phpunit',
                    'amount' => '1.150',
                    'operation' => 'incoming',
                    'order_id' => 1,
                ],
            ]);

        return $id;
    }

    /**
     * @depends testShow
     *
     * @param int $id
     *
     * @return mixed
     */
    public function testDelete(int $id)
    {
        $response = $this->deleteJson(route('api.order-payments.destroy', ['order_payment' => $id]));
        $response
            ->assertStatus(200)
            ->assertJson([
                'success' => true,
            ]);
    }
}
