<?php

namespace Tests\Api;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Foundation\Testing\TestCase as BaseTestCase;
use Laravel\Passport\Passport;
use Tests\CreatesApplication;

abstract class ApiTestCase extends BaseTestCase
{
    use CreatesApplication;

    protected $user = null;

    protected function setUp(): void
    {
        parent::setUp();
        $this->user = User::find(1);
        Passport::actingAs($this->user, ['*']);
        Controller::$fullInfo = false;
    }

    protected function tearDown(): void
    {
        parent::tearDown();
    }
}
